FROM rust:latest

ENV CARGO_ROOT /usr/local/cargo
ENV PATH $CARGO_ROOT/bin:$PATH

RUN rustup target add wasm32-unknown-unknown
RUN rustup component add clippy rustfmt
RUN cargo install cargo-audit trunk wasm-bindgen-cli wasm-pack \
    && rm -rf $CARGO_ROOT/registry $CARGO_ROOT/git

RUN apt update \
    && apt install -y firefox-esr chromium chromium-driver \
    && rm -rf /var/lib/apt/lists/*
